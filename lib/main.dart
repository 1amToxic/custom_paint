import 'package:custom_paint/custom_paint/custom_touchable_chart.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: SizedBox(
        height: double.infinity,
        width: double.infinity,
        child: TouchableChart(
          centerOffset: Offset(200, 200),
          canvasWidth: 300,
          canvasHeight: 300,
          strokeWidth: 10,
          summaryAngle: 220,
          backgroundColor: Color(0xFF30313C),
          borderColor: Color(0xFF404050),
          progressColor: Colors.orange,
        ),
      ),
    );
  }
}
