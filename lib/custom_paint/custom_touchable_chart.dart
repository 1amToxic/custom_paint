import 'package:flutter/material.dart';

import 'trackizer_paint.dart';

class TouchableChart extends StatefulWidget {
  final Offset centerOffset;
  final double canvasWidth;
  final double canvasHeight;
  final double summaryAngle;
  final double strokeWidth;
  final Color backgroundColor;
  final Color progressColor;
  final Color borderColor;

  const TouchableChart(
      {Key? key,
      required this.centerOffset,
      required this.canvasWidth,
      required this.canvasHeight,
      required this.summaryAngle,
      required this.strokeWidth,
      required this.backgroundColor,
      required this.progressColor,
      this.borderColor = Colors.transparent})
      : super(key: key);

  @override
  State<TouchableChart> createState() => _TouchableChartState();
}

class _TouchableChartState extends State<TouchableChart> {
  Offset progress = const Offset(0.0, 0.0);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (tapDownDetail) {
        setState(() {
          progress = tapDownDetail.globalPosition;
        });
      },
      onHorizontalDragUpdate: (dragHorizontalUpdate) {
        setState(() {
          progress = dragHorizontalUpdate.globalPosition;
        });
      },
      child: CustomPaint(
        painter: TrackizerPaint(
          progress: progress,
          backgroundColor: const Color(0xFF30313C),
          borderColor: const Color(0xFF404050),
          progressColor: Colors.orange,
          summaryAngle: 260,
          strokeWidth: 10,
          centerOffset: const Offset(200, 200),
          canvasWidth: 300,
          canvasHeight: 300,
        ),
      ),
    );
  }
}
