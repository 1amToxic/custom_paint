import 'dart:math';

import 'package:custom_paint/custom_paint/extension.dart';
import 'package:flutter/material.dart';
import 'package:vector_math/vector_math.dart' as math;

class ICustomPaint extends CustomPainter {
  double batteryProgress = 0;

  ICustomPaint({this.batteryProgress = 0});

  @override
  void paint(Canvas canvas, Size size) {
    if (batteryProgress == 0) {
      batteryProgress = 5;
    }
    //path
    final paint = Paint()
      ..strokeWidth = 5
      ..color = Colors.indigoAccent
      ..style = PaintingStyle.fill;

    final triangle = Path();
    triangle.moveTo(150, 0);
    triangle.relativeLineTo(100, 100);
    triangle.relativeLineTo(-150, 0);
    triangle.close();

    //circle
    final paintCircle = Paint()
      ..strokeWidth = 10
      ..color = Colors.yellow
      ..style = PaintingStyle.stroke;
    const circleCenter = Offset(100, 100);
    const circleRadius = 45.0;

    //arc
    final paintArc = Paint()
      ..strokeWidth = 6
      ..color = Colors.yellowAccent
      ..style = PaintingStyle.stroke;

    const arcCenter = Offset(200, 80);
    final arcRect = Rect.fromCircle(center: arcCenter, radius: 75);
    final startAngle = 10.0.degreeToRadians();
    final sweepAngle = -90.0.degreeToRadians();

    //curve
    final paintCurve = Paint()
      ..strokeWidth = 5
      ..color = Colors.red
      ..style = PaintingStyle.stroke;
    final qCurve2 = Path()
      ..moveTo(50, 50)
      ..relativeQuadraticBezierTo(150, 300, 300, 100);
    // canvas.drawPath(qCurve2, paintCurve);

    // canvas.drawPath(triangle, paint);
    //
    // canvas.drawCircle(circleCenter, circleRadius, paintCircle);
    //
    // canvas.drawArc(arcRect, startAngle, sweepAngle, false, paintArc);

    //battery
    final batteryPaintStroke = Paint()
      ..strokeWidth = 3
      ..color = Colors.black
      ..style = PaintingStyle.stroke;
    final batteryPaintFill = Paint()..color = Colors.black;
    final batteryPaintProgress = Paint()..color = Colors.green;
    final rrectProgress = RRect.fromRectAndRadius(
        Rect.fromLTWH(5, 55, batteryProgress, 140), const Radius.circular(20));
    final rrect = RRect.fromRectAndRadius(
        const Rect.fromLTWH(0, 50, 300, 150), const Radius.circular(20));
    const halfCircleCenter = Offset(310, 125);
    canvas.drawRRect(rrect, batteryPaintStroke);
    canvas.drawRRect(rrectProgress, batteryPaintProgress);
    canvas.drawArc(
        Rect.fromCircle(center: halfCircleCenter, radius: 35),
        -90.0.degreeToRadians(),
        180.0.degreeToRadians(),
        true,
        batteryPaintFill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) =>
      batteryProgress < 290;
}


