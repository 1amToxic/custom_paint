import 'dart:math';

import 'extension.dart';
import 'package:flutter/material.dart';

class TrackizerPaint extends CustomPainter {
  final Offset progress;
  final Offset centerOffset;
  final double canvasWidth;
  final double canvasHeight;
  final double summaryAngle;
  final double strokeWidth;
  final Color backgroundColor;
  final Color progressColor;
  final Color borderColor;

  TrackizerPaint({
    required this.progress,
    required this.centerOffset,
    required this.canvasWidth,
    required this.canvasHeight,
    required this.summaryAngle,
    required this.strokeWidth,
    required this.backgroundColor,
    required this.progressColor,
    required this.borderColor
  });

  @override
  void paint(Canvas canvas, Size size) {
    final minAngle = -(90 + summaryAngle / 2).degreeToRadians();
    final firstAngle = (360 - summaryAngle) / 2;
    final dx = progress.dx;
    final dy = progress.dy;
    final xx = centerOffset.dx - dx;
    final yy = centerOffset.dy - dy;
    double tanAngle = 0.0;
    double mulXY = xx * yy;
    tanAngle = yy.abs() / xx.abs();
    double angle = 0;
    if (mulXY < 0 && xx > 0) {
      angle = firstAngle.degreeToRadians() - atan(tanAngle);
      angle = angle.radianToDegree() > 0 ? angle : 0;
    } else if (mulXY > 0 && yy > 0) {
      angle = firstAngle.degreeToRadians() + atan(tanAngle);
    } else if (mulXY < 0 && xx < 0) {
      angle = (firstAngle + 90).degreeToRadians() + pi / 2 - atan(tanAngle);
    } else {
      angle = (firstAngle + 180).degreeToRadians() + atan(tanAngle);
      angle = angle.radianToDegree() <= summaryAngle
          ? angle
          : summaryAngle.degreeToRadians();
    }

    final paint = Paint()
      ..strokeWidth = strokeWidth
      ..color = backgroundColor
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;
    final paintBorder = Paint()
      ..strokeWidth = strokeWidth + 3
      ..color = borderColor
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;
    final paintProgress = Paint()
      ..strokeWidth = strokeWidth
      ..color = progressColor
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;
    var rRect = Rect.fromCenter(
        center: centerOffset, width: canvasWidth, height: canvasHeight);
    canvas.drawArc(
        rRect, minAngle, summaryAngle.degreeToRadians(), false, paintBorder);
    canvas.drawArc(
        rRect, minAngle, summaryAngle.degreeToRadians(), false, paint);
    canvas.drawArc(rRect, minAngle, angle, false, paintBorder);
    canvas.drawArc(rRect, minAngle, angle, false, paintProgress);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) =>
      (centerOffset.dy + canvasHeight / 2) >= progress.dy;
}
