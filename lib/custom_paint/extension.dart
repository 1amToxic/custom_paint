import 'dart:math';

extension DegreeToRadians on double {
  double degreeToRadians() => this * pi / 180;
  double radianToDegree() => this * 180 / pi;
}